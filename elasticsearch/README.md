Enabling Elasticsearch in GitLab:
https://docs.gitlab.com/ee/integration/elasticsearch.html#enabling-elasticsearch

GitLab Elasticsearch Knowledge:
https://docs.gitlab.com/ee/development/elasticsearch.html

GitLab Elasticsearch Troubleshooting:
https://docs.gitlab.com/ee/administration/troubleshooting/elasticsearch.html